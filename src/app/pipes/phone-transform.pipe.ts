import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'phoneTransform'
})
export class PhoneTransformPipe implements PipeTransform {

    transform(stringNumber: string): string {

        const countryCode = stringNumber.slice(0, 3);
        const areaCode = stringNumber.slice(3, 6);
        const lastSection = stringNumber.slice(6);

        return `( ${countryCode} ) ${areaCode} - ${lastSection}`;
    }

}
