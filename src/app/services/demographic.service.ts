import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {DemographicModel} from "../models/Demographic.model";

@Injectable({
    providedIn: 'root'
})
export class DemographicService {

    constructor(private http: HttpClient) {
    }

    getDemographicData(): Observable<DemographicModel> {
        return this.http.get<DemographicModel>('assets/demographics.json');
    }
}
