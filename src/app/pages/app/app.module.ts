import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HighchartsChartModule } from 'highcharts-angular';

import { AppComponent } from './app.component';
import { HoverBackgroundDirective } from '../../directives/hover-background.directive';
import { PhoneTransformPipe } from '../../pipes/phone-transform.pipe';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HighchartsChartModule,
  ],
  declarations: [AppComponent, HoverBackgroundDirective, PhoneTransformPipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
