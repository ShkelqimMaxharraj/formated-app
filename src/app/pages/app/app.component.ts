import {Component, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';
import * as lodash from 'lodash';
import {UserService} from "../../services/user.service";
import {DemographicService} from "../../services/demographic.service";
import {UserModel} from "../../models/User.model";
import {DemographicModel} from "../../models/Demographic.model";
import {Chart} from "highcharts";
import {Unsubscribe} from "../../helpers/UnsubscribeClass";
import {takeUntil} from "rxjs";

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent extends Unsubscribe implements OnInit {
    public currentUser: UserModel;
    public otherUsers: UserModel[];

    private chartRef;
    private demographics: DemographicModel[];
    public updateFlag = false;

    Highcharts: typeof Highcharts = Highcharts;
    chartOptions: Highcharts.Options = {
        title: {
            text: 'Demographics',
        },
        series: [],
    };

    constructor(private userService: UserService,
                private demographicService: DemographicService) {
        super();
    }

    public ngOnInit() {
        this.loadUserData();
        this.loadDemographicsData();
    }

    chartCallback: Highcharts.ChartCallbackFunction = (chart: Chart) => {
        this.chartRef = chart;
    };

    private loadUserData() {
        this.userService
            .getUserData()
            .pipe(takeUntil(this.destroyed$))
            .subscribe((users: UserModel[]) => {
                this.currentUser = users[0];
                this.otherUsers = users.filter((user, index) => index !== 0);
            });
    }

    private loadDemographicsData() {
        this.demographicService
            .getDemographicData()
            .pipe(takeUntil(this.destroyed$))
            .subscribe((demo) => {
                this.demographics = demo as unknown as DemographicModel[];
                this.initializeChartData();
            });
    }

    private static arrayZip = (a, b) => a.map((k, i) => [k, b[i]]);

    private initializeChartData() {
        const cityGroups = lodash.groupBy(this.demographics, 'city');

        Object.keys(cityGroups).forEach((city) => {
            const citySeries = {
                type: 'line',
                name: city,
                data: AppComponent.arrayZip(
                    cityGroups[city].map((d) => d.year),
                    cityGroups[city].map((d) => d.population)
                ),
            };

            this.chartRef.addSeries(citySeries);
        });
        this.updateFlag = true;
    }
}
