export interface DemographicModel {
    state: string;
    city: string;
    year: number;
    population: number;
}
