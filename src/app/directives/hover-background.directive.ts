import {Directive, ElementRef, HostListener, Input} from '@angular/core';
import {BackgroundTypeEnum} from "../enums/BackgroundType.enum";

@Directive({
    selector: '[appHoverBackground]'
})
export class HoverBackgroundDirective {
    @Input() phone: string;

    constructor(private el: ElementRef) {

    }

    @HostListener('mouseover')
    onMouseOver() {
        if (this.phone?.length !== 10)
            this.el.nativeElement.style.backgroundColor = BackgroundTypeEnum.LIGHTCORAL;
    }

    @HostListener('mouseout')
    onMouseOut() {
        this.el.nativeElement.style.backgroundColor = BackgroundTypeEnum.WHITE;
    }
}
